@wip
Feature: Savings report
  In order to know how much I saved on my purchases using the club card
  As a client
  I want to get a monthly report of the savings

  Background:
  	Given I have a Classic card

  Scenario: Card was used
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When I buy "icecream" in "heladeria A" establishment
    And the month ends 
    Then I should receive an email with "Savings report" as subject
    And the email should have "heladeria A" as establishment
    And the email should have "icecream" as product
    And the email should have "10" pounds of price cost
    And the email should have "1" pounds of benefit

  Scenario: Card was not used
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When I buy don't anything in this month
    And the month ends 
    Then I should not receive any email
