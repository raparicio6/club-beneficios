@wip
Feature: Congratulations email
  In order to know if I am the establishment that granted the most benefits in the past month
  As a establishment
  I want to get a congratulations email

  Background:
  	Given I am "heladeria A" establishment
    And there is "heladeria B" establishment

  Scenario: I gave the most benefits
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    And "heladeria B" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When "2" clients with Classic cards buy "icecream" in "heladeria A" establishment
    And "1" clients with Classic cards buy "icecream" in "heladeria B" establishment
    And the month ends 
    Then I should receive an email with "Congratulations" as subject

  Scenario: I didn't gave the most benefits
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    And "heladeria B" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When "1" clients with Classic cards buy "icecream" in "heladeria A" establishment
    And "2" clients with Classic cards buy "icecream" in "heladeria B" establishment
    And the month ends 
    Then I should not receive any email
