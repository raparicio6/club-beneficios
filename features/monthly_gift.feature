@wip
Feature: Monthly gift
  In order to know if I am the branch that granted the most benefits in the past month
  As a branch
  I want to get a gift

  Background:
  	Given there is "heladeria A" establishment
    And there is "heladeria B" establishment
    And I am "A1" branch of "heladeria A" establishment
    And there is "B1" branch of "heladeria B" establishment
    And there is "B2" branch of "heladeria B" establishment

  Scenario: I gave the most benefits
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    And "heladeria B" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When "3" clients with Classic cards buy "icecream" in "A1" branch
    And "2" clients with Classic cards buy "icecream" in "B1" branch
    And "2" clients with Classic cards buy "icecream" in "B2" branch
    And the month ends 
    Then I should receive a gift
  
  Scenario: I didn't gave the most benefits
    Given "heladeria A" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    And "heladeria B" establishment offers a "10" discount rate in "10" pounds "icecream" for Classic cards
    When "2" clients with Classic cards buy "icecream" in "A1" branch
    And "3" clients with Classic cards buy "icecream" in "B1" branch
    And the month ends 
    Then I should not receive any gift